inherit versionator
EXPORT_FUNCTIONS pkg_setup src_unpack src_compile src_install pkg_postinst

RESTRICT="binchecks strip primaryuri mirror"

DESCRIPTION="Bad ass kernel"

KEYWORDS="~x86 ~amd64"

LICENSE="GPL-2"

HOMEPAGE="http://www.zen-sources.org"

#Eclass functions only from here onwards ...
#==============================================================
detect_version() {
	if [[ -n ${KV_FULL} ]]; then
		# we will set this for backwards compatibility.
		KV=${KV_FULL}

		# we know KV_FULL so lets stop here. but not without resetting S
		S=${WORKDIR}/linux-${KV_FULL}
		return
	fi
	CKV=${CKV:-${PV}}
	OKV=${OKV:-${CKV}}
	OKV=${OKV/_rc/-rc}
	OKV=${OKV/-r*}

	KV_MAJOR=$(get_version_component_range 1 ${OKV})
	KV_MINOR=$(get_version_component_range 2 ${OKV})
	KV_PATCH=$(get_version_component_range 3- ${OKV})

	KV_PATCH=${KV_PATCH/[-_]*}

	KERNEL_URI="mirror://kernel/linux/kernel/v${KV_MAJOR}.${KV_MINOR}/linux-${OKV}.tar.bz2"

	RELEASE=${CKV/${OKV}}
	RELEASE=${RELEASE/_rc/-rc}
	RELEASETYPE=${RELEASE//[0-9]}

	ZEN_REV=$(echo ${PR} | cut -b2)
	ZEN_HOTFIX_REV=$(echo ${PR} | cut -b3)

	#hackish, but should be obvious as to why this has been changed.
	if [ "${PN}" == "zendisk-sources" ]; then
	PATCHSET="-zendisk"
	COMPRESSTYPE=".lzma"
	else
		if [ "${PN}" == "zenrt-sources" ]; then
			PATCHSET="-zenrt"
			COMPRESSTYPE=".bz2"
		else
			if [ "${PN}" == "zenmm-sources" ]; then
				PATCHSET="-zenmm"
				COMPRESSTYPE=".bz2"
			else
				PATCHSET="-zen"
				COMPRESSTYPE=".bz2"
			fi
		fi
	fi
	EXTRAVERSION="${RELEASE}${PATCHSET}${ZEN_REV}.${ZEN_HOTFIX_REV}"

	KV_FULL=${OKV}${EXTRAVERSION}

	S=${WORKDIR}/linux-${KV_FULL}
	KV=${KV_FULL}

	if [[ ${RELEASETYPE} == -rc ]]; then
		OKV="${KV_MAJOR}.${KV_MINOR}.$((${KV_PATCH} - 1))"
		KERNEL_URI="mirror://kernel/linux/kernel/v${KV_MAJOR}.${KV_MINOR}/testing/patch-${CKV//_/-}.bz2
					mirror://kernel/linux/kernel/v${KV_MAJOR}.${KV_MINOR}/linux-${OKV}.tar.bz2"
		PREPATCH="patch-${CKV//_/-}"
	else
		KERNEL_URI="mirror://kernel/linux/kernel/v${KV_MAJOR}.${KV_MINOR}/linux-${CKV//_/-}.tar.bz2"
	fi
	ZEN_PATCH="${PV/_rc/-rc}${PATCHSET}${ZEN_REV}.patch${COMPRESSTYPE}"
	if [ "${PN}" == "zendisk-sources" ]; then
	ZEN_PATCH_URI="http://disktech.net/kernel/${ZEN_PATCH}"
	else
	ZEN_PATCH_URI="http://zen-sources.org/files/${ZEN_PATCH}"
	fi
        KERNEL_URI="${KERNEL_URI} ${ZEN_PATCH_URI}"
	if [[ ${ZEN_HOTFIX_REV} > 0 ]]; then
		for ((i=1;i<=${ZEN_HOTFIX_REV};i+=1)); do
			ZEN_HOTFIX_PATCH="${PV/_rc/-rc}${PATCHSET}${ZEN_REV}-hotfix${i}.patch${COMPRESSTYPE}"
			if [ "${PN}" == "zendisk-sources" ]; then
			ZEN_HOTFIX_URI="http://disktech.net/kernel/${ZEN_HOTFIX_PATCH}"
			else
			ZEN_HOTFIX_URI="http://zen-sources.org/files/${ZEN_HOTFIX_PATCH}"
			fi
	        	KERNEL_URI="${KERNEL_URI} ${ZEN_HOTFIX_URI}"
		done
	fi		
	SRC_URI="${KERNEL_URI}"
}

PROVIDE="virtual/linux-sources virtual/alsa"

SLOT="${PVR}"
IUSE="symlink"

# Unpack functions
#==============================================================
universal_unpack() {
	cd ${WORKDIR}
	unpack linux-${OKV}.tar.bz2
	[[ -n ${PREPATCH} ]] && unpack ${PREPATCH}.bz2
	unpack ${ZEN_PATCH}
        if [[ ${ZEN_HOTFIX_REV} > 0 ]]; then
                for ((i=1;i<=${ZEN_HOTFIX_REV};i+=1)); do
			HOTFIX_PATCH="${PV/_rc/-rc}${PATCHSET}${ZEN_REV}-hotfix${i}.patch${COMPRESSTYPE}"
			unpack ${HOTFIX_PATCH}
		done
	fi
	if [[ -d "linux" ]]; then
		mv linux linux-${KV_FULL} \
			|| die "Unable to move source tree to ${KV_FULL}."
	elif [[ "${OKV}" != "${KV_FULL}" ]]; then
		mv linux-${OKV} linux-${KV_FULL} \
			|| die "Unable to move source tree to ${KV_FULL}."
	fi
	cd "${S}"

	# remove all backup files
	find . -iname "*~" -exec rm {} \; 2> /dev/null

	# fix a problem on ppc where TOUT writes to /usr/src/linux breaking sandbox
	# Failed patch my ass.
#	sed -i \
#		-e 's|TOUT	:= .tmp_gas_check|TOUT	:= $(T).tmp_gas_check|' \
#		"${S}"/arch/ppc/Makefile
}

unpack_set_extraversion() {
	cd "${S}"
	sed -i -e "s:^\(EXTRAVERSION =\).*:\1 ${EXTRAVERSION}:" Makefile
	cd "${OLDPWD}"
}

# Should be done after patches have been applied
# Otherwise patches that modify the same area of Makefile will fail
unpack_fix_install_path() {
	cd "${S}"
	sed	-i -e 's:#export\tINSTALL_PATH:export\tINSTALL_PATH:' Makefile
}

# install functions
#==============================================================
install_universal() {
	#fix silly permissions in tarball
	cd ${WORKDIR}
	chown -R root:0 *
	chmod -R a+r-w+X,u+w *
	cd ${OLDPWD}
}

install_sources() {
	local file

	cd "${S}"
	dodir /usr/src
	echo ">>> Copying sources ..."

	mv ${WORKDIR}/linux* ${D}/usr/src
}

# pkg_postinst functions
#==============================================================
postinst_sources() {
	local MAKELINK=0

	# if we have USE=symlink, then force K_SYMLINK=1
	use symlink && K_SYMLINK=1

	# if we are to forcably symlink, delete it if it already exists first.
	if [[ ${K_SYMLINK} > 0 ]]; then
		[[ -h ${ROOT}usr/src/linux ]] && rm ${ROOT}usr/src/linux
		MAKELINK=1
	fi

	# if the link doesnt exist, lets create it
	[[ ! -h ${ROOT}usr/src/linux ]] && MAKELINK=1

	if [[ ${MAKELINK} == 1 ]]; then
		cd ${ROOT}usr/src
		ln -sf linux-${KV_FULL} linux
		cd ${OLDPWD}
	fi

	# Don't forget to make directory for sysfs
	[[ ! -d ${ROOT}sys ]] && kernel_is 2 6 && mkdir ${ROOT}sys

	# if K_EXTRAEINFO is set then lets display it now
	if [[ -n ${K_EXTRAEINFO} ]]; then
		echo ${K_EXTRAEINFO} | fmt |
		while read -s ELINE; do	einfo "${ELINE}"; done
	fi

	# if K_EXTRAEWARN is set then lets display it now
	if [[ -n ${K_EXTRAEWARN} ]]; then
		echo ${K_EXTRAEWARN} | fmt |
		while read -s ELINE; do ewarn "${ELINE}"; done
	fi
}

# zenpatch
#==============================================================
zenpatch() {
	cd ${S}
	if [[ -n ${PREPATCH} ]]; then
        	echo ">>> Applying -rc prepatch ... "
        	patch -p1 -s < ${WORKDIR}/${PREPATCH}
	fi
       	echo ">>> Applying ${PATCHSET} patch ... "
	patch -p1 -s < ${WORKDIR}/${ZEN_PATCH/${COMPRESSTYPE}/}
        if [[ ${ZEN_HOTFIX_REV} > 0 ]]; then
                for ((i=1;i<=${ZEN_HOTFIX_REV};i+=1)); do
                        HOTFIX_PATCH="${PV/_rc/-rc}${PATCHSET}${ZEN_REV}-hotfix${i}.patch${COMPRESSTYPE}"
	       		echo ">>> Applying -hotfix${i} ... "
			patch -p1 -s < ${WORKDIR}/${HOTFIX_PATCH/${COMPRESSTYPE}/}
                done
	fi
}

# common functions
#==============================================================
zen-sources_src_unpack() {
	universal_unpack

	zenpatch

	unpack_set_extraversion
	unpack_fix_install_path

	cd "${S}"
}

zen-sources_src_compile() {
	cd "${S}"
}

zen-sources_src_install() {
	install_universal
	install_sources
}

zen-sources_pkg_postinst() {
	postinst_sources
}

zen-sources_pkg_setup() {
	ABI="${KERNEL_ABI}"
	echo ">>> Preparing to unpack ..."
}

